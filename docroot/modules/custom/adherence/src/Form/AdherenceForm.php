<?php

namespace Drupal\adherence\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a Adherence form.
 */
class AdherenceForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'campaign_adherence';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $campaign_id = NULL) {
    $values = [
      'type' => 'campaign',
      'nid' => $campaign_id,
    ];

    // Get the nodes.
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties($values);

    if (empty($node)) {
      return $form;
    }
    else {
      $node = $node[$campaign_id];
    }

    $campaign_title = $node->get('title')->getValue()[0]['value'];
    $campaign_description = $node->get('field_campaign_description')->getValue()[0]['value'];
    $campaign_vaccines_id = $node->get('field_vaccines')->getValue();
    $campaign_vaccines = Node::loadMultiple(array_values(array_column($campaign_vaccines_id, 'target_id')));
    $vaccines = [];
    foreach ($campaign_vaccines as $vaccine) {
      $title = $vaccine->get('title')->getValue()[0]['value'];
      $vaccines[$vaccine->get('nid')->getValue()[0]['value']] = $title;
    }
    $form['#title'] = $campaign_title;
    $form['campaign_id'] = [
      '#type' => 'value',
      '#value' => $campaign_id,
    ];
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<span>' . $campaign_description . '</span>',
    ];

    $form['vaccines'] = [
      '#type' => 'radios',
      '#title' => $this->t('Vaccine options'),
      '#options' => $vaccines,
      '#required' => TRUE,
    ];

    $form['policy_terms'] = [
      '#type' => 'checkbox',
      '#prefix' => 'Accept',
      '#title' => '<span>I read and accept the <a href="/policy"> terms</a> and conditions </span>',
      '#default_value' => FALSE,
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('<front>');
    $campaign_id = $form_state->getValue('campaign_id');
    $vaccine_id = $form_state->getValue('vaccines');
    $uid = \Drupal::currentUser()->id();
    $adherenceList = \Drupal::service('adherence.load');
    $adherenceList->setAdhesion($campaign_id, $vaccine_id, $uid);
    $this->messenger()->addStatus($this->t('The action has been successfully saved.'));
    $form_state->setRedirect('adherence.list');
  }

}
