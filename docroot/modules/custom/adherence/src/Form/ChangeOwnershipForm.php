<?php

namespace Drupal\adherence\Form;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a Adherence form.
 */
class ChangeOwnershipForm extends FormBase {

  /**
   * The id of the adhrence.
   *
   * @var int
   */
  public int $adherenceId = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'adherence_change_ownership';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $adherence_id = NULL) {
    $adherenceList = \Drupal::service('adherence.load');
    $adherence = $adherenceList->getAdhesionId($adherence_id);
    $adherence = json_decode(json_encode($adherence[0]), TRUE);
    $campaign = Node::load($adherence['id_campaing']);
    $vaccine = Node::load($adherence['id_vaccine']);
    $campaign_description = $campaign->get('field_campaign_description')->getValue()[0]['value'];

    $this->adherenceId = $adherence_id;

    $form['#title'] = $campaign->getTitle();
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<span>' . $campaign_description . '</span>',
    ];
    $form['vaccine'] = [
      '#type' => 'textfield',
      '#title' => "Vaccine",
      '#value' => $vaccine->getTitle(),
      '#attributes' => [
        'disabled' => TRUE,
      ],
    ];

    $form['login'] = [
      '#type' => 'textfield',
      '#title' => 'Login',
      '#autocomplete_route_name' => 'adherence.autocomplete.users',
    ];

    $form['policy_terms'] = [
      '#type' => 'checkbox',
      '#prefix' => 'Accept',
      '#title' => '<span>I read and accept the <a href="/policy"> terms</a> and conditions </span>',
      '#default_value' => FALSE,
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancel'],
      '#button_type' => 'danger',
      '#limit_validation_errors' => [],
    ];
    $form['actions']['cancel']['#attributes']['class'][] = 'btn-danger';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('login'));
    $current_id = \Drupal::currentUser()->id();
    $adherenceList = \Drupal::service('adherence.load');
    $adherenceList->updateChangeOwnership($this->adherenceId, $uid);
    $this->messenger()->addStatus($this->t('The Adhrence has been updated.'));
    $form_state->setRedirect('adherence.list', ['uid' => $current_id]);
  }

  /**
   * Form submission handler for the 'cancel' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function cancel(array $form, FormStateInterface $form_state) {
    $current_id = \Drupal::currentUser()->id();
    $form_state->setRedirect('adherence.list', ['uid' => $current_id]);
  }

}
