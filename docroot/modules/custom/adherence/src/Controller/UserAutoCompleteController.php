<?php

namespace Drupal\adherence\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\user\Entity\User;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class UserAutoCompleteController extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStroage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse([]);
    }

    $input = Xss::filter($input);
    $uid = \Drupal::currentUser()->id();

    $select = Database::getConnection()->select('user__field_base', 'fb');
    $select->fields('fb', ['entity_id', 'field_base_value']);
    $select->condition('fb.entity_id', $uid);
    $results = $select->execute();
    $user_base = $results->fetchAll()[0];
    $user_base = json_decode(json_encode($user_base), TRUE);
    $state = explode("- ", $user_base['field_base_value'])[1];

    $select = Database::getConnection()->select('user__field_base', 'fb');
    $select->fields('fb', ['entity_id']);
    $select->join('users_field_data', 'us', 'fb.entity_id = us.uid');
    $select->condition('fb.field_base_value', '%' . $state . '%', 'LIKE');
    $select->condition('us.name', '%' . $input . '%', 'LIKE');
    $results = $select->execute();
    $ids = json_decode(json_encode($results->fetchAll()), TRUE);

    $user_ids = [];
    foreach ($ids as $id) {
      $user_ids[] = $id['entity_id'];
    }

    $users = User::loadMultiple($user_ids);

    $results = [];
    foreach ($users as $user) {
      $label = [
        $user->get('name')->getValue()[0]['value'],
        '<small>(' . $user->id() . ')</small>',
      ];
      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$user]),
        'label' => implode(' ', $label),
      ];
    }

    return new JsonResponse($results);
  }

}
