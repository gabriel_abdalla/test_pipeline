<?php

namespace Drupal\adherence\Controller;

use Drupal\campaign\Helper\CampaignHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Adherence routes.
 */
class DataExportController extends ControllerBase {

  /**
   * Export the campaign information data. TESTE DOC.
   */
  public function exportCampaignData(Request $request) {
    $adherence_service = \Drupal::service('adherence.load');
    $campaign_id = $request->get('id');

    $campaign_details = $adherence_service->getCampaignDetails($campaign_id);
    if (empty($campaign_details)) {
      \Drupal::messenger()->addMessage($this->t('No data available to export'), MessengerInterface::TYPE_STATUS);
      return $this->redirect('campaign.list.details', ['id' => $campaign_id]);
    }

    $file_name = $campaign_details[0]->title;
    $file_name = str_replace(' ', '_', $file_name);
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $file_name = $file_name . '-' . $date->format('Y-m-d');

    $rows = [];
    foreach ($campaign_details as $register) {
      $vaccine_name = $this->entityTypeManager()
        ->getStorage('node')
        ->load($register->id_vaccine)->getTitle();
      $user_base = $register->field_base_value;
      $user_name = $register->name;
      $user_apply_date = date('d/M/y', $register->apply_date);
      $row = [
        'user_name' => $user_name,
        'campaign_title' => $user_base,
        'vaccine_title' => $vaccine_name,
        'apply_date' => $user_apply_date,
      ];
      $rows[] = $row;
    }

    $this->setResponseHeaders($file_name);

    $column_names = FALSE;
    // Run loop through each row in $rows.
    foreach ($rows as $row) {
      if (!$column_names) {
        echo implode("\t", array_keys($row)) . "\n";
        $column_names = TRUE;
      }
      array_walk($row, [$this, 'filterData']);
      echo implode("\t", array_values($row)) . "\n";
    }
    die();
  }

  /**
   * Export the campaign information data.
   */
  public function exportConsolidatedCampaignsData() {
    $campaigns = new CampaignHelper();
    $campaignsData = $campaigns->getCampaignsHelper();

    if (empty($campaignsData)) {
      \Drupal::messenger()->addMessage($this->t('No data available to export'), MessengerInterface::TYPE_STATUS);
      return $this->redirect('campaign.list', []);
    }

    $file_name = 'Consolidated_Data';
    $file_name = str_replace(' ', '_', $file_name);
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $file_name = $file_name . '-' . $date->format('Y-m-d');

    $rows = [];
    foreach ($campaignsData as $register) {
      $title = $register[0];
      $status = $register[1];
      $vaccine_names = $register[2];
      $vaccine_adhrence_MG = $register[3];
      $vaccine_adhrence_SP = $register[4];
      $vaccines_campaign_data = '';
      for ($i = 0; $i < count($vaccine_names); $i++) {
        $vaccine_name = $vaccine_names[$i];
        $total_by_vaccine = $vaccine_adhrence_MG[$i] + $vaccine_adhrence_SP[$i];
        $vaccines_campaign_data .= $vaccine_name . ' - ' . $total_by_vaccine . '; ';
      }
      $sum = $register[5];
      $row = [
        'campaign_title' => $title,
        'status' => $status,
        'vaccine_title' => $vaccines_campaign_data,
        'adherence_mg' => array_sum($vaccine_adhrence_MG),
        'adherence_sp' => array_sum($vaccine_adhrence_SP),
        'sum' => $sum,
      ];
      $rows[] = $row;
    }

    $this->setResponseHeaders($file_name);

    $column_names = FALSE;
    // Run loop through each row in $rows.
    foreach ($rows as $row) {
      if (!$column_names) {
        echo implode("\t", array_keys($row)) . "\n";
        $column_names = TRUE;
      }
      array_walk($row, [$this, 'filterData']);
      echo implode("\t", array_values($row)) . "\n";
    }
    die();

  }

  /**
   * Export the campaign information data.
   */
  public function exportRawCampaignsData() {
    $adherence_service = \Drupal::service('adherence.load');
    $campaigns = new CampaignHelper();
    $campaignsData = $campaigns->getCampaignsHelper();

    if (empty($campaignsData)) {
      \Drupal::messenger()->addMessage($this->t('No data available to export'), MessengerInterface::TYPE_STATUS);
      return $this->redirect('campaign.list', []);
    }

    $file_name = 'Raw_Data';
    $file_name = str_replace(' ', '_', $file_name);
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $file_name = $file_name . '-' . $date->format('Y-m-d');

    $rows = [];
    foreach ($campaignsData as $register) {
      $campaign_details = $adherence_service->getCampaignDetails($register[6]);
      foreach ($campaign_details as $details) {
        $vaccine_name = $this->entityTypeManager()
          ->getStorage('node')
          ->load($details->id_vaccine)->getTitle();
        $user_base = $details->field_base_value;
        $user_name = $details->name;
        $campaign_title = $details->title;
        $user_apply_date = date('d/M/y', $details->apply_date);
        $status = $register[1];
        $row = [
          'user_name' => $user_name,
          'user_base' => $user_base,
          'campaign_title' => $campaign_title,
          'vaccine_title' => $vaccine_name,
          'apply_date' => $user_apply_date,
          'status' => $status,
        ];
        $rows[] = $row;

      }
    }

    $this->setResponseHeaders($file_name);

    $column_names = FALSE;
    // Run loop through each row in $rows.
    foreach ($rows as $row) {
      if (!$column_names) {
        echo implode("\t", array_keys($row)) . "\n";
        $column_names = TRUE;
      }
      array_walk($row, [$this, 'filterData']);
      echo implode("\t", array_values($row)) . "\n";
    }
    die();

  }

  /**
   * Filter the campaign information data.
   */
  public function filterData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"')) {
      $str = '"' . str_replace('"', '""', $str) . '"';
    }
  }

  /**
   * Export the campaign information data.
   */
  public function setResponseHeaders(string $file_name) {
    header('Content-Description: File Transfer');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . $file_name . '.xls"');
  }

}
