<?php

namespace Drupal\adherence\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Returns responses for Adherence routes.
 */
class AdherenceController extends ControllerBase {

  /**
   * Load the adherence data.
   */
  public function load() {
    $uid = $this->currentUser()->id();
    $adherenceList = \Drupal::service('adherence.load');
    $list = $adherenceList->getAdhesions($uid);

    $header = [
      $this->t('User name'),
      $this->t('Campaign Title'),
      $this->t('Vaccine Title'),
      $this->t('Campaign Status'),
      $this->t('Operations'),
    ];
    $rows = [];
    foreach ($list as $item) {
      $item = json_decode(json_encode($item), TRUE);
      $operations = [
        'data' => [
          '#type' => 'operations',
          '#links' => [],
        ],
      ];
      $status = ucfirst(explode('_', $item['field_campaign_sta_value'])[2]);
      if ($status == 'Change') {
        $operations['data']['#links']['change_ownership'] = [
          'title' => $this->t('Change Ownership'),
          'url' => Url::fromRoute('adherence.change_ownership', ['adherence_id' => $item['id']]),
        ];
      }
      $row = [
        $item['name'],
        $item['title'],
        $item['nv_title'],
        ucfirst(explode('_', $item['field_campaign_sta_value'])[2]),
        $operations,
      ];
      $rows[] = $row;
    }

    $build['adhesions'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No feeds available'),
    ];
    $build['adhesions']['#attributes']['class'][] = 'views-table custom-table';
    $build['adhesions']['#attached']['library'][] = 'campaign/table_style';
    return $build;
  }

}
