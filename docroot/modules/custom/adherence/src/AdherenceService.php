<?php

namespace Drupal\adherence;

use Drupal\Core\Database\Database;

/**
 * Defines a Service for managing vaccine adherence.
 */
class AdherenceService {

  /**
   * Constructor.
   */
  public function __construct() {}

  /**
   * Get Adhesions for a user.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   List of all adhesions for that user.
   */
  public function getAdhesions($uid) {
    $select = Database::getConnection()->select('adherence', 'ad');
    $select->fields('ad', ['uid', 'id']);
    $select->condition('ad.uid', $uid);
    $select->join('node_field_data', 'nd', 'ad.id_campaing = nd.nid');
    $select->join('node_field_data', 'nv', 'ad.id_vaccine = nv.nid');
    $select->join('users_field_data', 'us', 'ad.uid = us.uid');
    $select->join('node__field_campaign_sta', 'cs', 'ad.id_campaing = cs.entity_id');
    $select->addField('us', 'name');
    $select->addField('nd', 'title');
    $select->addField('nv', 'title');
    $select->addField('cs', 'field_campaign_sta_value');
    $select->orderBy('nd.title');
    $select->orderBy('us.name');
    $results = $select->execute();
    return $results->fetchAll();
  }

  /**
   * Insert an Adhesions for a user.
   */
  public function setAdhesion($campaign_id, $vaccine_id, $uid) {
    $date = new \DateTime();
    $time_stamp = $date->getTimestamp();
    $insert = Database::getConnection()->insert('adherence');
    $insert->fields([
      'id_campaing',
      'id_vaccine',
      'uid',
      'apply_date',
    ], [
      $campaign_id,
      $vaccine_id,
      $uid,
      $time_stamp,
    ]);
    $insert->execute();
  }

  /**
   * Get Adhesions for a campaign.
   */
  public function getAdhesionsCampaign($campaign_id) {
    $select = Database::getConnection()->select('adherence', 'ad');
    $select->fields('ad', ['id_campaing']);
    $select->condition('ad.id_campaing', $campaign_id);
    $results = $select->execute();
    return $results->fetchCol();
  }

  /**
   * Get all Adhesions.
   */
  public function getAdhesionsVaccine($vaccine_id, $campaign_id) {
    $select = Database::getConnection()->select('adherence', 'ad');
    $select->fields('ad', ['id', 'id_campaing', 'id_vaccine', 'uid']);
    $select->condition('ad.id_vaccine', $vaccine_id);
    $select->condition('ad.id_campaing', $campaign_id);
    $results = $select->execute();
    return $results->fetchAll();
  }

  /**
   * Get all Adhesions.
   */
  public function getCampaignDetails($campaign_id) {
    $select = Database::getConnection()->select('adherence', 'ad');
    $select->fields('ad', [
      'id',
      'id_campaing',
      'id_vaccine',
      'uid',
      'apply_date',
    ]);
    $select->join('users_field_data', 'u_data', 'ad.uid = u_data.uid');
    $select->join('user__field_base', 'base', 'ad.uid = base.entity_id');
    $select->join('node_field_data', 'node_data', 'ad.id_campaing = node_data.nid');
    $select->addField('node_data', 'title');
    $select->addField('u_data', 'name');
    $select->addField('base', 'field_base_value');
    $select->condition('ad.id_campaing', $campaign_id);
    $results = $select->execute();
    return $results->fetchAll();
  }

  /**
   * Get Adherence by id.
   */
  public function getAdhesionId($adherence_id) {
    $select = Database::getConnection()->select('adherence', 'ad');
    $select->fields('ad', ['id', 'id_campaing', 'id_vaccine', 'uid']);
    $select->condition('ad.id', $adherence_id);
    $results = $select->execute();
    return $results->fetchAll();
  }

  /**
   * Update function for the Change Ownership feature.
   */
  public function updateChangeOwnership($adherence_id, $uid) {
    $connection = Database::getConnection();
    $connection->update('adherence')
      ->fields([
        'uid' => $uid,
      ])
      ->condition('id', $adherence_id)
      ->execute();
  }

}
