<?php

namespace Drupal\campaign\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\campaign\Helper\CampaignHelper;

/**
 * Returns responses for Adherence routes.
 */
class CampaignController extends ControllerBase {

  /**
   * Load the adherence data.
   */
  public function getCampaigns() {
    $campaigns = new CampaignHelper();
    $campaignsData = $campaigns->getCampaignsHelper();

    $header = [
      "Campaign",
      "Status",
      "Vaccine Name",
      "MG",
      "SP",
      "Sum",
    ];
    $rows = [];

    foreach ($campaignsData as $campaign) {
      $row = [];
      $title = $campaign[0];
      $status = $campaign[1];
      $url = Url::fromRoute('campaign.list.details', ['id' => $campaign[6]])->toString();
      $url_markup = Markup::create('<a href=' . $url . '>' . $title . '</a>');
      $row = [
        $url_markup,
        $status,
      ];
      $sum = $campaign[5];
      $vaccine_titles = "<ul>";
      $vaccine_adhrence_MG = "<ul>";
      $vaccine_adhrence_SP = "<ul>";

      for ($i = 0; $i < count($campaign[2]); $i++) {
        $vaccine_titles .= "<li>" . $campaign[2][$i] . "</li><hr>";
        $vaccine_adhrence_MG .= "<li>" . $campaign[3][$i] . "</li><hr>";
        $vaccine_adhrence_SP .= "<li>" . $campaign[4][$i] . "</li><hr>";
      }
      $row[] = Markup::create($vaccine_titles . "</ul>");
      $row[] = Markup::create($vaccine_adhrence_MG . "</ul>");
      $row[] = Markup::create($vaccine_adhrence_SP . "</ul>");
      $row[] = $sum;
      $rows[] = $row;
    }

    $build['adhesions'] = [
      '#prefix' => $this->renderAllCampaignsBtns(),
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No campaigns'),
    ];
    $build['adhesions']['#attributes']['class'][] = 'views-table custom-table';
    $build['adhesions']['#attached']['library'][] = 'campaign/table_style';
    return $build;
  }

  /**
   * Load the details of a campaign.
   */
  public function getCampaignDetail(Request $request) {
    $adherence_service = \Drupal::service('adherence.load');
    $campaign_id = $request->get('id');

    $header = [
      "Vaccine",
      "Base",
      "Login",
      "Apply date",
    ];

    $people_registered = $adherence_service->getCampaignDetails($campaign_id);
    $rows = [];
    foreach ($people_registered as $register) {
      $vaccine_name = $this->entityTypeManager()
        ->getStorage('node')
        ->load($register->id_vaccine)->getTitle();
      $user_base = $register->field_base_value;
      $user_name = $register->name;
      $user_apply_date = date('d/M/y', $register->apply_date);
      $row = [
        $vaccine_name,
        $user_base,
        $user_name,
        $user_apply_date,
      ];
      $rows[] = $row;
    }

    $build['adhesions'] = [
      '#prefix' => $this->renderCampaignDetailsBtns($campaign_id),
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No one registered in this campaign'),
    ];
    $build['adhesions']['#attributes']['class'][] = 'views-table custom-table';
    $build['adhesions']['#attached']['library'][] = 'campaign/table_style';
    return $build;

  }

  /**
   * Load the details of a campaign.
   */
  public function renderCampaignDetailsBtns($campaign_id) {
    $back_url = Url::fromRoute('campaign.list', [])->toString();
    $export_data_url = Url::fromRoute('adherence.data_export', ['id' => $campaign_id])->toString();
    return '<a href="' . $back_url . '" class="button button-action button--primary button--small">Back</a><a href="' . $export_data_url . '" class="button button-action button--primary button--small">Export Data</a>';
  }

  /**
   * Load the details of all campaigns.
   */
  public function renderAllCampaignsBtns() {
    $back_url = $_SERVER["HTTP_REFERER"];
    ;
    $export_consolidated_data_url = Url::fromRoute('campaign.list.export_consolidated')->toString();
    $export_raw_data_url = Url::fromRoute('campaign.list.export_raw')->toString();
    return '<a href="' . $back_url . '" class="button button-action button--primary button--small">Back</a>
    <a href="' . $export_consolidated_data_url . '" class="button button-action button--primary button--small">Export Consolidated Data</a>
    <a href="' . $export_raw_data_url . '" class="button button-action button--primary button--small">Export Raw Data</a>';
  }

}
