<?php

namespace Drupal\campaign\Helper;

use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;

/**
 * Helpr class to load campaign data.
 */
class CampaignHelper {

  /**
   * Construct function.
   */
  public function __construct() {

  }

  /**
   * Load the adherence data.
   */
  public function getCampaignsHelper() {
    $adherenceList = \Drupal::service('adherence.load');
    $rows = [];

    $nodes = \Drupal::entityTypeManager()->getStorage('node')
      ->loadByProperties(['type' => 'campaign']);

    foreach ($nodes as $node) {
      $row = [];
      $title = $node->getTitle();
      $status = $node->get('field_campaign_sta')->getValue()[0]['value'];
      $vaccines = $node->get('field_vaccines')->getValue();
      $row = [
        $title,
        ucfirst(explode("_", $status)[2]),
      ];
      $sum = 0;
      $vaccine_titles = [];
      $vaccine_adhrence_MG = [];
      $vaccine_adhrence_SP = [];
      foreach ($vaccines as $vaccine) {
        $adherences = $adherenceList->getAdhesionsVaccine($vaccine['target_id'],
        $node->id());
        $bases = [
          "MG" => 0,
          "SP" => 0,
        ];
        foreach ($adherences as $adherence) {
          $adherence = json_decode(json_encode($adherence), TRUE);
          $select = Database::getConnection()->select('user__field_base', 'fb');
          $select->fields('fb', ['entity_id', 'field_base_value']);
          $select->condition('fb.entity_id', $adherence['uid']);
          $results = $select->execute();
          $user_base = $results->fetchAll()[0];
          $user_base = json_decode(json_encode($user_base), TRUE);
          $state = explode("- ", $user_base['field_base_value'])[1];
          $bases[$state]++;
        }
        $vaccine_name = Node::load(intval($vaccine['target_id']))->getTitle();
        $sum += $bases["MG"] + $bases["SP"];
        $vaccine_titles[] = $vaccine_name;
        $vaccine_adhrence_MG[] = $bases["MG"];
        $vaccine_adhrence_SP[] = $bases["SP"];
      }
      $row[] = $vaccine_titles;
      $row[] = $vaccine_adhrence_MG;
      $row[] = $vaccine_adhrence_SP;
      $row[] = $sum;
      $row[] = $node->id();
      $rows[] = $row;
    }
    return $rows;
  }

}
