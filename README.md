# Drupal Internship Training Project
## Instructions

### Run This Project Locally

With lando installed, do:  
- Make a repository fork;  
- Clone this fork;  
- In the terminal go to the project folder and runs `lando start`  
- Install drupal in the interface
- Go to `/admin/config/user-interface/shortcut/manage/default/customize` and delete both sortcuts
- run `lando drush cset system.site uuid 0663f418-85c4-4a5e-afff-6c0fcc2dd75b`
- run `lando drush cim`

### Git Flow
Add a remote branch to the original repository to fetch the changes
```
$ git remote add <remote_name> git@bitbucket.org:<upstream>/<repo>.git
```

Get the changes made in the master branch
```
$ git fetch <remote_name>
$ git checkout master
$ git merge <remote_name>/master
```

Sample:
 1. Create a new branch to work;  
 2. Make changes;  
 3. Commit your changes;  
 4. Push your branch `git push -u origin <your_branch>`  

To our project:  

```
$ git remote add upstream git@bitbucket.org:gabriel_abdalla/test_pipeline.git
$ git fetch upstream
$ git checkout master
$ git merge upstream/master
```

updating your forked repository

```
$ git push
```

### Credentials

URL: http://dev01fxbph6ecmv.devcloud.acquia-sites.com/