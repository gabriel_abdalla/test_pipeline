#!/bin/bash
composer install
export PIPELINES_ENV=PIPELINES_ENV

vendor/bin/phpcs --config-set installed_paths ../../drupal/coder/coder_sniffer
vendor/bin/phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml docroot/modules/custom